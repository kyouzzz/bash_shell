#!/bin/bash
# 本地的修改上传到测试 server

function echoVersion(){
    dir_name=$1
    http_confs=(
        "masterapi-international=>root@67.228.120.91:/usr/local/zend/etc/sites.d/vhost_http_masterserver.sbt.naeu.heroesofnewerth.com_80.conf"
        "internal-api-intl=>root@67.228.120.91:/usr/local/zend/etc/sites.d/vhost_http_internal-api.sbt.naeu.heroesofnewerth.com_80.conf"
    )
    for i in "${http_confs[@]}" ; do
        # 从左截取到 =>
        dir_curr="${i%%=>*}"
        if [[ $dir_name == $dir_curr ]]; then
            # 从右截取到 =>
            conf="${i##*=>}"
            break;
        fi
    done
    if [[ $conf ]]; then
        conf=(${conf//:/ })
        host=${conf[0]}
        vhost_file=${conf[1]}
        content=`ssh $host "grep 'DocumentRoot' $vhost_file "`
    fi
    echo $content
}

project_base_dir=`git rev-parse --show-toplevel`
base_dir_name=`basename $project_base_dir`

if [[ $1 == '-t' ]]; then
    echoVersion $base_dir_name
    exit
fi

# 文件夹对应的 remote path
declare -a remote_mapping
remote_mapping=(
    "events=>root@203.117.148.102:/var/www/events/"
    "admin-international=>root@203.117.148.100:/usr/local/zend/var/apps/http/internal-admin.sbt.naeu.heroesofnewerth.com/80/3.7.13.5_12/public_html/"
    "masterapi-international=>root@67.228.120.91:/usr/local/zend/var/apps/http/masterserver.sbt.naeu.heroesofnewerth.com/80/3.9.14.27_9569/"
    "library-international=>root@67.228.120.91:/usr/local/zend/var/libraries/library_sbt/default/"
    "library-international=>root@67.228.120.91:/usr/local/zend/var/apps/http/internal-api.sbt.naeu.heroesofnewerth.com/80/3.9.14.25_9570/vendor/jingle/library-international"
    "internal-api-intl=>root@67.228.120.91:/usr/local/zend/var/apps/http/internal-api.sbt.naeu.heroesofnewerth.com/80/3.9.14.25_9570/"
)

# remote_mapping=(
#     "masterapi-international=>root@111.223.63.12:/usr/local/zend/var/apps/http/masterserver.rct.sea.heroesofnewerth.com/80/3.9.12.18_193/"
#     "library-international=>root@111.223.63.12:/usr/local/zend/var/libraries/library_rct/default/"
# )


for i in "${remote_mapping[@]}" ; do
    # 从左截取到 =>
    KEY="${i%%=>*}"
    if [[ $base_dir_name == $KEY ]]; then
        # 从右截取到 =>
        remote="$remote  ${i##*=>}  "
    fi
done

if [[ ! $remote ]]; then
    echo "Remote not found. exit! "
    exit
fi

# 获取当前修改文件 未 commit
uncommit_f_list=`git status --porcelain`;
# 获取已 commit 修改文件
commit_f_list=`git diff --name-status $1 $2`
f_list="$commit_f_list
$uncommit_f_list"

declare -a add_files 

while read line
do
    arr=($line)
    mod=${arr[0]}
    f=${arr[1]}
    change_flag=${arr[2]}
    r_f=${arr[3]}

    if [[ $mod == *D ]]; then
        if [[ ! "${delete_files[@]}" =~ "${f}" ]]; then
            delete_files+=($f);
        fi
        # 从添加里面删除
        if [[ "${add_files[@]}" =~ "${f}" ]]; then
            remove_file=($f)
            add_files=("${add_files[@]/$remove_file}")
        fi
    fi

    if [[ $mod == *M || $mod == *A ]]; then
        if [[ ! "${add_files[@]}" =~ "${f}" ]]; then
            add_files+=($f);
        fi
        # 从添加里面删除
        if [[ "${delete_files[@]}" =~ "${f}" ]]; then
            add_file=($f)
            delete_files=("${delete_files[@]/$add_file}")
        fi
    fi
    # 修改名称
    if [[ $mod == *R ]]; then
        if [[ ! "${delete_files[@]}" =~ "${f}" ]]; then
            delete_files+=($f);
        fi
        # 从添加里面删除
        if [[ "${add_files[@]}" =~ "${f}" ]]; then
            remove_file=($f)
            add_files=("${add_files[@]/$remove_file}")
        fi

        # 添加 r_f
        if [[ ! "${add_files[@]}" =~ "${r_f}" ]]; then
            add_files+=($r_f);
        fi
        # 从添加里面删除
        if [[ "${delete_files[@]}" =~ "${r_f}" ]]; then
            add_file=($r_f)
            delete_files=("${delete_files[@]/$add_file}")
        fi

    fi

done  <<< "$f_list"

printf "REMOTE :  \e[1;32m$remote \e[0m\n"

echo "新增|修改 :"
for i in "${add_files[@]}"; do
    if [[ $i ]]; then
        printf  "\e[1;32m ++++ $i\e[0m\n"
    fi
done

echo "删除 :"
for i in "${delete_files[@]}"; do
    if [[ $i ]]; then
        printf  "\e[1;31m ---- $i\e[0m\n"
    fi    
done


read -p "Are you sure to send theses files to server? [y|n]" choice

case "$choice" in 
    y|Y ) 
        file_str=""
        for i in "${add_files[@]}"; do
            if [[ $i ]]; then
                file_str="$file_str $i"
            fi
        done
        # muti remote server
        for r in $remote; do
            rsync -aRv $file_str $r
        done
        ;;
    n|N )
        echo "Abort.";;
    * ) echo "Invalid";;
esac


