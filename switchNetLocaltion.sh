#!/bin/bash
# 登录自动切换位置

wifi_name=`networksetup -getairportnetwork en0 | awk '{print $NF}'`

echo `date`  "   " $wifi_name >> /tmp/kyou_wifi.log 
if [ $wifi_name == "JL-Office-WiFi" ]; then
    scselect "Garena"
else
    scselect "Home"
fi
